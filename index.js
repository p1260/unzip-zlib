const { join } = require("path");
const { auto } = require("async");
const { dirSync } = require("tmp");
const extract = require("extract-zip");
const { writeFile, rename, readFileSync, chmod } = require("fs");

const main = (rtfzip, callback) => {
  const tmpOptions = {}.tmpOptions || {};
  const tempDir = dirSync({
    prefix: "unzip_",
    unsafeCleanup: true,
    ...tmpOptions
  });

  return auto(
    {
      writeFile: (callback) =>
        writeFile(join(tempDir.name, "source.rtf"), rtfzip, "latin1", callback),
      unzipFile: (callback) => {
        extract(join(tempDir.name, `source.rtf`), {
          dir: tempDir.name
        })
          .then((err, done) => {
            callback(null);
          })
          .catch((err) => {
            callback(new Error(err));
          });
      },
      renameFile: [
        "writeFile",
        "unzipFile",
        (err, callback) => {
          return rename(
            join(tempDir.name, `-`),
            join(tempDir.name, `uzip.rtf`),
            (err) => {
              if (err) {
                return callback(new Error(err));
              }

              return callback(null);
            }
          );
        }
      ],
      returnFile: [
        "renameFile",
        (res, callback) => {
          chmod(join(tempDir.name, `uzip.rtf`), 0o777, (ee) => {
            if (ee) callback(new Error(ee));

            callback(null, readFileSync(join(tempDir.name, `uzip.rtf`)));
          });
        }
      ]
    },
    (err, results) => {
      tempDir.removeCallback();

      if (err) {
        return callback(err);
      }

      return callback(null, results.returnFile);
    }
  );
};

module.exports = { main };
